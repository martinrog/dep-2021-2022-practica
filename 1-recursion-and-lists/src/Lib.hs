{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- TODO: Schrijf en documenteer de functie ex1, die de som van een lijst getallen berekent.
-- Voorbeeld: ex1 [3,1,4,1,5] ~> 14

{-- | Deze functie is recursief (Roept zichzelf opnieuw aan).Te beginnen met een basecase,
waar bij een lege lijst 0 wordt terug gegeven. Vervolgens hakt hij de lijst op 
in de eerste item van de list (x) + de rest van de lijst (xs). Daarna geeft hij de opdracht om
 x + (functie opnieuw aanroepen maar dan met xs (ipv de hele lijst)). Zo krijg je uiteindelijk een som van alle
 items in een lijst.
--}

ex1 :: [Int] -> Int
ex1[] = 0
ex1(x:xs) = x + ex1 xs

-- TODO: Schrijf en documenteer de functie ex2, die alle elementen van een lijst met 1 ophoogt.
-- Voorbeeld: ex2 [3,1,4,1,5] ~> [4,2,5,2,6]
{-- | Deze functie verhoogt elk getal uit een lijst met 1. Dit gebeurd ongeveer op dezelfde manier als hierboven,
alleen ipv dat bij de recursie alles bij elkaar wordt opgeteld, blijft de lijst in tact en wordt er bij de x één
opgeteld (m.b.v. de recursie). (Basecase geeft trouwens geen 0 meer, maar een lege lijst)
--}
ex2 :: [Int] -> [Int]
ex2[] = []
ex2 (x:xs) = x + 1: ex2 xs

-- TODO: Schrijf en documenteer de functie ex3, die alle elementen van een lijst met -1 vermenigvuldigt.
-- Voorbeeld: ex3 [3,1,4,1,5] ~> [-3,-1,-4,-1,-5]
{-- | Deze functie vermenigvuldigt elk getal uit een lijst met -1. Zelfde verhaal als hierboven weer.
Alleen nu wordt er iets anders met de x gedaan. Deze wordt met -1 vermenigvuldigt. Zo zorg je ervoor
dat alle items uit de list negatief worden. 
--}
ex3 :: [Int] -> [Int]
ex3[] = []
ex3 (x:xs) = x * (-1): ex3 xs

-- TODO: Schrijf en documenteer de functie ex4, die twee lijsten aan elkaar plakt.
-- Voorbeeld: ex4 [3,1,4] [1,5] ~> [3,1,4,1,5]
-- Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. 
-- Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
{-- | Deze functie plakt twee aparte lijsten aan elkaar vast. De base cases zijn hier iets anders want als
een van beide lijsten leeg is, dan geeft hij de ander terug. Of als ze beide leeg zijn, dan geeft hij een lege 
terug. Daarna loopt hij door de eerste lijst heen met recursie. Eigenlijk op dezelfde wijze als dat het bij
de voorgaande functies doet. 

--}
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] [] = []
ex4 (x:xs) [] = x:xs
ex4 [] (x:xs) = x:xs
ex4 (x:xs) (lijst2) = x: ex4 xs (lijst2)


-- TODO: Schrijf en documenteer een functie, ex5, die twee lijsten van gelijke lengte paarsgewijs bij elkaar optelt.
-- Voorbeeld: ex5 [3,1,4] [1,5,9] ~> [4,6,13]
{-- | Deze functie telt uit twee aparte lijsten elk getal, paarsgewijs, bij elkaar op. Dit doet hij door de 
het eerste item van lijst 1 (x) + het eerste item uit lijst 2 (y) te doen. vervolgens dit herhalen met de rest
van de lijsten totdat hij de basecase bereikt.
--}
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 (x:xs) [] = x:xs
ex5 [] (x:xs) = x:xs
ex5 (x:xs) (y:ys) = x + y : ex5 xs ys


-- TODO: Schrijf en documenteer een functie, ex6, die twee lijsten van gelijke lengte paarsgewijs met elkaar vermenigvuldigt.
-- Voorbeeld: ex6 [3,1,4] [1,5,9] ~> [3,5,36] 
{-- | Deze functie vermenigvuldigt uit twee aparte lijsten elk getal met elkaar, ook dit gebeurd paarsgewijs.
Ook deze functie gebeurd op dezelfde manier als hierboven. Alleen worden de x en y met elkaar vermenigvuldigt ipv
opgeteld. 
--}
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 (x:xs) [] = x:xs
ex6 [] (x:xs) = x:xs
ex6 (x:xs) (y:ys) = x * y : ex6 xs ys

-- TODO: Schrijf en documenteer een functie, ex7, die de functies ex1 en ex6 combineert tot een functie die het inwendig product uitrekent.
-- Voorbeeld: ex7 [3,1,4] [1,5,9] geeft 3*1 + 1*5 + 4*9 = 44 terug als resultaat.
{-- | Deze functie berekent het inwendig product van twee lijsten. Dit gebeurd door eerst de functie ex6 op de
lijsten toe te passen. Dus alles paarsgewijs vermenigvuldigen. En de nieuwe lijst vervolgens toe te passen in de
functie ex1. Deze telt alle bij elkaar op. Zo heb je het inwendig product van twee lijsten.
--}

ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1(ex6 x y)
